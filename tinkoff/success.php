<?php

ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);
set_error_handler('exceptions_error_handler', E_ALL);

require_once (dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/wp-blog-header.php');

require_once ('TinkoffMerchantAPI.php'); // TkbMerchantAPI

function getPluginOptions()
{
    $options = get_option('woocommerce_tinkoff_settings', []);

    return $options;
}

function getPaymentDetailsByVendor($vendorId)
{
    $payment_methods = get_user_meta($vendorId, 'dokan_profile_settings', true);
    $payment_methods['payment']['bank']['last_name'] = get_user_meta($vendorId, 'last_name', true);
    $payment_methods['payment']['bank']['first_name'] = get_user_meta($vendorId, 'first_name', true);

    return $payment_methods['payment']['bank'];
}

function impossiblePay($orderId)
{
    //записать в заказ и оповестить продавца и админа о невозможности платежа
    $order = wc_get_order($orderId);
    $order->add_order_note('Платеж получен, но не был вам переведен, проверьте ваши платежные данные');
}

function getFormattedPaymentInfo($parentOrders)
{
    $paymentArray = [];

    if ($parentOrders != false) {
        foreach ($parentOrders as $orderId) {
            $order = wc_get_order($orderId);
            /*
            OrderID string да Уникальный идентификатор операции
            Amount long да Сумма пополнения в копейках
            Account string да Номер счета получателя (Клиента мерчанта)
            Bik string да БИК банка получателя
            Inn string да ИНН получателя
            Name string да ФИО получателя
            Description string да Описание перевода (назначение платежа)
            */
            $paymentDetails = getPaymentDetailsByVendor(get_post_meta($orderId, '_dokan_vendor_id', true));
            $paymentArray[$orderId] = [
                'OrderID' => $orderId,
                'Amount' => round(floatval($order->get_data()['total']) * 100),
                'Account' => $paymentDetails['ac_number'],
                'Bik' => $paymentDetails['iban'],
                'Inn' => $paymentDetails['swift'],
                'Name' => $paymentDetails['last_name'] . ' ' . $paymentDetails['first_name'],
                'Description' => 'Оплата по заказу ' . $orderId,
                //'vendorId' => intval(get_post_meta($orderId, '_dokan_vendor_id', true)),
            ];
            foreach ($paymentArray[$orderId] as $key => $val) {
                if (strlen($val) == 0) {
                    impossiblePay($orderId);
                    unset($paymentArray[$orderId]);
                }
            }
        }
    }

    return $paymentArray;
}

function getParentOrdersInfo($orderId)
{
    $sub_orders = get_children([
            'post_parent' => $orderId,
            'post_type' => 'shop_order',
            'post_status' => [
                'wc-pending',
                'wc-completed',
                'wc-processing',
                'wc-on-hold'
            ]
        ]
    );

    return count($sub_orders) > 0 ? getFormattedPaymentInfo(array_keys($sub_orders)) : false;
}

function exceptions_error_handler($errno, $errstr, $errfile, $errline)
{
    if (error_reporting() == 0 || $errno != E_ERROR) {
        return;
    }

    die('NOTOK1'); // ошибка в работе CMS
}

try {
    require(dirname(__FILE__) . '../../../../../wp-blog-header.php');
    $request = (array) json_decode(file_get_contents('php://input'), true);

    header("HTTP/1.1 200 ok");

    $headers = apache_request_headers();

    $headers = getallheaders();
    file_put_contents(dirname(__FILE__) . "/../logs/header.log", $headers, FILE_APPEND);

    //TODO: check's
        $options = getPluginOptions();

        $orders = $wpdb->get_results(
            "SELECT * FROM " . $wpdb->prefix . "woocommerce_order_items 
                WHERE order_id=" . (int)$request['OrderInfo']['OrderId']
        );

        $order_status = $wpdb->get_results(
                "SELECT * FROM " . $wpdb->prefix . "posts 
                    WHERE ID=" . $orders[0]->order_id);
                    
        $status = $order_status[0]->post_status;
        $order = wc_get_order((int)$request['OrderInfo']['OrderId']);

        switch ($request['OrderInfo']['State']) {
            case 0:
            case 1:
            case 2:
                if ($status == 'wc-pending') {
                    $order_status = 'wc-on-hold';
                    $order->update_status($order_status);
                    die('OK');
                }
                break; /*Деньги на карте захолдированы. Корзина очищается.*/
            case 3:
                $order_status = 'wc-processing';
                $parentOrders = getParentOrdersInfo($request['OrderInfo']['OrderId']);
                if (count($parentOrders) > 0) {
                    foreach ($parentOrders as $parentOrder)
                    {
                        
                        $TAPI = new TinkoffMerchantAPI (
                            $options['merchant_id'], 
                            $options['secret_key']
                        );

                        $request = $TAPI->buildQuery('Init', $parentOrder);
                        $requestArr = json_decode($request, true);
                        if (intval($requestArr['orderNumber']) > 0) {
                            $ordern = wc_get_order($parentOrder['OrderID']);
                            $ordern->add_order_note('Вам был отправлен платеж за заказ: ' . $requestArr['orderNumber']);
                            $ordern->update_status('wc-completed');
                        }
                    }
                }
                break; /*Платеж подтвержден.*/
            case 'REJECTED':
                $order_status = 'wc-failed';
                break; /*Платеж отклонен.*/
            case 'CANCELED':
                /*case 6:
                    $order_status = 'wc-cancelled';
                    break; */
            case 6:
                $order_status = 'wc-refunded';
                break; /*Произведен возврат денег клиенту*/
        }

        $order->update_status($order_status);

        die('OK');
} catch (Exception $e) {
    die('NOTOK3'); // ошибка в работе модуля
}
